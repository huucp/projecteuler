import java.util.Arrays;

/**
 * Created by huupc on 28/04/2017.
 */
public class LexicographicPermutations_24 {
    /**
     * A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
     * <p>
     * 012   021   102   120   201   210
     * <p>
     * What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
     */
    public static void main(String[] args) {
        int[] permutation = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int n = 1000000;
        //int[] permutation = new int[]{1, 2, 3, 4};
        //int n = 24;
        int count = 1;
        while (count <n && !isLastPermutation(permutation)) {
            permutation = nextPermutation(permutation);
            count++;
//            System.out.println(Arrays.toString(permutation));
        }
        System.out.println(Arrays.toString(permutation));
    }

    private static int[] nextPermutation(int[] previousPermutation) {
        if (isLastPermutation(previousPermutation)) return previousPermutation;
        int[] clone = previousPermutation.clone();
        // Find the largest index k such that a[k] < a[k + 1]. If no such index exists, the permutation is the last permutation.
        int k = -1, l = -1;
        int length = previousPermutation.length;
        for (int i = length - 2; i >= 0; i--) {
            if (previousPermutation[i] < previousPermutation[i + 1]) {
                k = i;
                break;
            }
        }
        l = k + 1;
        // Find the largest index l greater than k such that a[k] < a[l].
        for (int i = length - 1; i > k; i--) {
            if (previousPermutation[k] < previousPermutation[i]) {
                l = i;
                break;
            }
        }
        // Swap the value of a[k] with that of a[l].
        int temp = clone[k];
        clone[k] = clone[l];
        clone[l] = temp;

        //  Reverse the sequence from a[k + 1] up to and including the final element a[n].
        reverseArray(clone, k + 1, length - 1);
        return clone;
    }

    // Find the largest index k such that a[k] < a[k + 1]. If no such index exists, the permutation is the last permutation.
    private static boolean isLastPermutation(int[] permutation) {
        for (int i = permutation.length - 1; i > 0; i--) {
            if (permutation[i] > permutation[i - 1]) {
                return false;
            }
        }
        System.out.println("last permutation:");
        System.out.println(Arrays.toString(permutation));
        return true;
    }

    private static void reverseArray(int[] array, int startIndex, int endIndex) {
        int half = startIndex + ((endIndex + 1) - startIndex) / 2;
        int endCount = endIndex;
        for (int startCount = startIndex; startCount < half; startCount++) {
            int store = array[startCount];
            array[startCount] = array[endCount];
            array[endCount] = store;
            endCount--;
        }
    }


    /** SOLUTION BY md2perpe. THE FACT: there are n! permutations when we have n elements to be ordered
     * The method I used was principally that explained by euler:

     Let's enumerate the permutations starting with index 0. Then were going to find the permutation with index 999999. The  permutation with index 0 is 0123456789.

     We shall also enumerate the digits "available for use" in the same way, i.e. starting with index 0. At the beginning, those digits will be 0123456789 (in that order).

     We now write 999999 = 2 * 9! + 274239.
     The quotient 2 gives the index (in 0123456789) of the first digit: 2.
     Remove that digit from the available digits: 013456789.

     Next, we write 274239 = 6 * 8! + 32319.
     The quotient 6 again gives the index (now in 013456789) of the second digit: 7.
     Remove that digit from the available digits: 01345689.

     Continue in this way, dividing by n!, until (including) n=0.
     The quotients will be (from the beginning): 2, 6, 6, 2, 5, 1, 2, 1, 1 and 0, giving the digits 2, 7, 8, 3, 9, 1, 5, 4, 6 and 0.

     The searched-for permutation thus is: 2783915460.

     */
}
