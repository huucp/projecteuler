/**
 * Created by huupc on 26/04/2017.
 */
public class SpecialPythagoreanTriplet {
    /**
     * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
     * <p>
     * a^2 + b^2 = c^2
     * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
     * <p>
     * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
     * Find the product abc.
     */
    public static void main(String[] args) {
        int c;
        for (int a = 1; a < 1000; a++) {
            for (int b = a + 1; b < 1000; b++) {
                c = 1000 - a - b;
//                if ((a * a + b * b - c * c) == 0) {
                if ((Math.pow(a, 2) + Math.pow(b, 2)) == Math.pow(c, 2)) {
                    System.out.println(a + "\t" + b + "\t" + c);
                    System.out.println(a * b * c);
                    return;
                }
            }
        }
        System.out.println("No solution");

//        long a, b, c, k, l, m;
//        int flag = 0;
//        for (a = 1; a < 1000; a++) {
//            if (flag == 1) {
//                break;
//            }
//            for (b = 1; b < 1000; b++) {
//                for (c = 1; c < 1000; c++) {
//                    k = a * a;
//                    l = b * b;
//                    m = c * c;
//                    if (a + b + c == 1000 && k + l == m) {
//
//                        System.out.println(a + "\t" + b + "\t" + c);
//                        System.out.println(a * b * c);
//                        flag++;
//                    }
//
//                }
//            }
//
//        }


    }
}
