/**
 * Created by huupc on 26/04/2017.
 */
public class SumSquareDifference {
    public static void main(String[] args) {
        System.out.println(sumSquareDifferene(100));
    }

    public static int sumSquareDifferene(int maxN){
        int sum = 0;
        int sumSquare =0;
        for (int i =1; i <= maxN;i++) {
            sum+=i;
            sumSquare+= i*i;
        }
        return Math.abs(sumSquare - sum * sum);
    }
}
