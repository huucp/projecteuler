import java.util.ArrayList;
import java.util.List;

/**
 * Created by huupc on 25/04/2017.
 */
public class EvenFiboNumber {
    public static void main(String[] args) {
        System.out.println(new EvenFiboNumber().calSum(4000000));
    }

    private List<Integer> fibo = new ArrayList<>();

    public int calSum(int n) {
        fibo.add(1);
        int next = 2;
        int count = 2;
        int sum =0;
        do {
            if (next%2==0) sum+=next;
            fibo.add(next);
            next = fibo.get(count - 2) + fibo.get(count - 1);
            count++;
        } while (next < n);
        return sum;
    }
}
