import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by huupc on 05/05/2017.
 */
public class PandigitalProducts_21 {

    public static final String ZERO = "0";

    /**
     * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.
     * <p>
     * The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.
     * <p>
     * Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
     * <p>
     * HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
     */
    public static void main(String[] args) {
        long sum =0;
        List<Integer> list = new ArrayList<>();
        for (int i = 2; i <= 99; i++) {
            for (int j = 123; j <= 9999; j++) {
                int prod = i * j;
                if (i * j < 1000 || i * j > 9999) continue;
                if (!list.contains(prod) && isPandigital(i, j, prod)) {
                    System.out.printf("%d x %d = %d\n",i,j,prod);
                    sum+=prod;
                    list.add(prod);
                }
            }
        }
        System.out.println(sum);
    }

    private static boolean isPandigital(int a, int b, int c) {
        String as = Integer.toString(a);
        String bs = Integer.toString(b);
        String cs = Integer.toString(c);
        if (as.contains(ZERO) || bs.contains(ZERO) || cs.contains(ZERO)) return false;
        if (as.length() + bs.length() + cs.length() != 9) return false;
        Set<Character> set = new HashSet<>();
        for (Character character : as.toCharArray()) {
            set.add(character);
        }
        for (Character character : bs.toCharArray()) {
            set.add(character);
        }
        for (Character character : cs.toCharArray()) {
            set.add(character);
        }
        if (set.size() != 9) return false;
        return true;
    }
}
