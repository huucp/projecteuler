import java.util.ArrayList;
import java.util.List;

/**
 * Created by huupc on 26/04/2017.
 */
public class SummationOfPrimes {
    /**
     * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
     * <p>
     * Find the sum of all the primes below two million.
     */
    public static void main(String[] args) {
        List<Integer> primes = new ArrayList<>();
        primes.add(2);
        primes.add(3);
        long sum = 2;
        for (int lastPrime = 3; lastPrime < 2000000; lastPrime += 2) {
            boolean isPrime = true;
            double sqrt = Math.sqrt(lastPrime);
            for (int p : primes) {
                if (p > sqrt) break;
                if (lastPrime % p == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                sum += lastPrime;
                primes.add(lastPrime);
            }
        }
        System.out.println(sum);
    }

}
