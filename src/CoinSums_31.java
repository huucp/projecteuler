/**
 * Created by huupc on 05/05/2017.
 */
public class CoinSums_31 {
    /**
     * In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
     * <p>
     * 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
     * It is possible to make £2 in the following way:
     * <p>
     * 1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
     * How many different ways can £2 be made using any number of coins?
     */
    public static void main(String[] args) {
        sol2();
    }

    private static void sol1() {
        int count = 0;
        for (int twoPound = 0; twoPound <= 1; twoPound++) {
            for (int onePound = 0; onePound <= 2; onePound++) {
                for (int fiftyPence = 0; fiftyPence <= 5; fiftyPence++) {
                    for (int twentyPence = 0; twentyPence <= 10; twentyPence++) {
                        for (int tenPence = 0; tenPence <= 20; tenPence++) {
                            for (int fivePence = 0; fivePence <= 40; fivePence++) {
                                for (int twoPence = 0; twoPence <= 100; twoPence++) {
                                    for (int onePence = 0; onePence <= 200; onePence++) {
                                        if ((twoPound * 200 + onePound * 100 + fiftyPence * 50 + twentyPence * 20 +
                                                tenPence * 10 +
                                                fivePence * 5 + twoPence * 2
                                                + onePence) == 200) {
                                            count++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(count);
    }

    private static void sol2() {
        int target = 200;
        int[] coinSizes = {1, 2, 5, 10, 20, 50, 100, 200};
        int[] ways = new int[target + 1];
        ways[0] = 1;
        for (int i = 0; i < coinSizes.length; i++) {
            for (int j = coinSizes[i]; j <= target; j++) {
                ways[j] += ways[j - coinSizes[i]];
            }
        }
        System.out.println(ways[target]);
    }
}
