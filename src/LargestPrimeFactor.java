import java.util.ArrayList;
import java.util.List;

/**
 * Created by huupc on 25/04/2017.
 */
public class LargestPrimeFactor {
    public static void main(String[] args) {
        System.out.println(new LargestPrimeFactor().findLargestPrimeFactor(4));
        System.out.println(new LargestPrimeFactor().findLargestPrimeFactor(5));
        System.out.println(new LargestPrimeFactor().findLargestPrimeFactor(6));
        System.out.println(new LargestPrimeFactor().findLargestPrimeFactor(140));
        System.out.println(new LargestPrimeFactor().findLargestPrimeFactor(600851475143L));

    }

    public long findLargestPrimeFactor(long n) {
        if (n < 2) return 0;
        int largestPrime = 1;
        List<Integer> primes = new ArrayList<>();
        primes.add(2);
        do {
            largestPrime = nextPrime(largestPrime, primes);
            n = extractPrime(n, largestPrime);
        } while (n > 1);
        return largestPrime;
    }

    private int nextPrime(int lastPrime, List<Integer> primes) {
        if (lastPrime==1) return 2;
        boolean isPrime = false;
        int p = lastPrime;
        do {
            if (lastPrime==2) p+=1;
            else p += 2;
            isPrime = true;
            for (int i : primes) {
                if (p % i == 0) {
                    isPrime = false;
                    break;
                }
            }
        }
        while (!isPrime);
        primes.add(p);
        return p;

    }

    private long extractPrime(long n, int p) {
        while (n % p == 0) {
            n = n / p;
        }
        return n;
    }
}
