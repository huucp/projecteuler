/**
 * Created by huupc on 26/04/2017.
 */
public class LongestCollatzSequence_14 {
    /**
     * The following iterative sequence is defined for the set of positive integers:
     * <p>
     * n → n/2 (n is even)
     * n → 3n + 1 (n is odd)
     * <p>
     * Using the rule above and starting with 13, we generate the following sequence:
     * <p>
     * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
     * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
     * <p>
     * Which starting number, under one million, produces the longest chain?
     * <p>
     * NOTE: Once the chain starts the terms are allowed to go above one million.
     */
    public static void main(String[] args) {
        int maxChain = 4;
        int n = 1;
        for (int i = 1; i < 1000000; i++) {
            int count = findSequenceCount(i);
            System.out.println(i + " - " + count);
            if (count > maxChain) {
                maxChain = count;
                n = i;
            }

        }
        System.out.println(n);
        System.out.println(maxChain);
    }

    private static int findSequenceCount(long n) {
        int count = 0;
        do {
            double log = Math.log(n) / Math.log(2);
            if (log == (int) log) {
                return count + (int) log + 1;
            }
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = 3 * n + 1;
            }
            count++;
        } while (n != 1);
        return count;
    }

}
