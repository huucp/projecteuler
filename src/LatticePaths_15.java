import java.util.Arrays;
/**
 * Created by huupc on 26/04/2017.
 */
public class LatticePaths_15 {
    /**
     * Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
     * How many such routes are there through a 20×20 grid?
     */
    public static void main(String[] args) {
        int n=20;
        long[][] a = new long[n+1][n+1];
        //a[18][18] = 2;
        a[n-1][n] = 1;
        a[n][n-1] = 1;
        a[n][n] = 1;

        for (int i = n-1; i >= 0; i--) {
            a[i][n] = 1;
            a[n][i] = 1;
            for (int j = n-1; j >= i; j--) {
                a[i][j] = a[i][j + 1] + a[i + 1][j];
                a[j][i] = a[i][j];
            }
        }

        for (long[] aa :
                a) {
            System.out.println(Arrays.toString(aa));
        }

        System.out.println(a[0][0]);


// solution 1: combination of number of down and number of right (d+r)!/(d!*r!)
        long result = 1;
        for ( int i = 1 ; i <= n ; i++ ) {
            result *= (i+n);
            result /= i;
        }
        System.out.println(result);

        // solution 2:
        long matrix[][] = new long[n][n];
        for (int i = 0; i < n; i++) {
            matrix[i][0] = i + 2;
            matrix[0][i] = i + 2;
        }
        for (int i = 1; i < n; i++) {
            for (int j = i; j < n; j++) {      // j>=i
                matrix[i][j] = matrix[i - 1][j] + matrix[i][j - 1];
                matrix[j][i] = matrix[i][j];   // avoids double computation (difference)
            }
        }
        System.out.println(matrix[n - 1][n - 1]);
    }
}
