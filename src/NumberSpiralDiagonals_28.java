/**
 * Created by huupc on 03/05/2017.
 */
public class NumberSpiralDiagonals_28 {
    /**
     * Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

     21 22 23 24 25
     20  7  8  9 10
     19  6  1  2 11
     18  5  4  3 12
     17 16 15 14 13

     It can be verified that the sum of the numbers on the diagonals is 101.

     What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
     */
    public static void main(String[] args) {
        System.out.println(sumSprialDiagonals(5));
        System.out.println(sumSprialDiagonals(1001));

    }

    private static long sumSprialDiagonals(int n){
        int half =(n-1)/2;
        long sum = 1;
        int lastCornerValue =1;
        for (int i = 1; i <=half;i++){
            for (int j = 1; j <=4;j++){
                lastCornerValue += i*2;
                sum+=lastCornerValue;
            }
        }
        return sum;
    }
}
