import java.math.BigInteger;

/**
 * Created by huupc on 28/04/2017.
 */
public class ReciprocalCycles_26 {
    /**
     * A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

     1/2	= 	0.5
     1/3	= 	0.(3)
     1/4	= 	0.25
     1/5	= 	0.2
     1/6	= 	0.1(6)
     1/7	= 	0.(142857)
     1/8	= 	0.125
     1/9	= 	0.(1)
     1/10	= 	0.1
     Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

     Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
     */
    /**
     * SOLUTION :http://mathworld.wolfram.com/DecimalExpansion.html
     * Any nonregular fraction m/n is periodic, and has a period lambda(n) independent of m, which is at most n-1 digits
     * long. If n is relatively prime to 10, then the period lambda(n) of m/n is a divisor of phi(n) and has at most
     * phi(n) digits, where phi is the totient function. It turns out that lambda(n) is the multiplicative order of
     * 10 (mod n) (Glaisher 1878, Lehmer 1941). The number of digits in the repeating portion of the decimal expansion
     * of a rational number can also be found directly from the multiplicative order of its denominator.
     */
    /**
     * http://www.mathblog.dk/project-euler-26-find-the-value-of-d-1000-for-which-1d-contains-the-longest-recurring-cycle/
     */

    public static void main(String[] args) {
        int sequenceLength = 0;

        for (int i = 1000; i > 1; i--) {
            if (sequenceLength >= i) {
                break;
            }

            int[] foundRemainders = new int[i];
            int value = 1;
            int position = 0;

            while (foundRemainders[value] == 0 && value != 0) {
                foundRemainders[value] = position;
                value *= 10;
                value %= i;
                position++;
            }

            if (position - foundRemainders[value] > sequenceLength) {
                sequenceLength = position - foundRemainders[value];
            }
        }
        System.out.println(sequenceLength);
    }

    private static int FindRecurringCycle(int n){
        int k = 1;
        BigInteger nB = BigInteger.valueOf(n);
        BigInteger prod = BigInteger.ONE;
        while (prod.multiply(BigInteger.TEN).remainder(nB).compareTo(BigInteger.ONE)!=0){
            k++;
        }
        return k;
    }

    private static int ExtractAll2And5(int n){
        while (n%2==0) n = n/2;
        while (n%5==0) n = n/5;
        return n;
    }

    private static boolean isCoprime(int a, int b) {
        return gcd(a, b) == 1;
    }

    private static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}
