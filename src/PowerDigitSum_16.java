import java.math.BigInteger;

/**
 * Created by huupc on 27/04/2017.
 */
public class PowerDigitSum_16 {
    /**
     * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

     What is the sum of the digits of the number 2^1000?
     */
    public static void main(String[] args) {
        BigInteger two = BigInteger.valueOf(2);
        BigInteger prod = BigInteger.ONE;
        for (int i = 0; i < 1000;i++) {
            prod = prod.multiply(two);
        }
        long sum = 0;

        String prodS = prod.toString();
        for (int i = 0; i < prodS.length(); i++) {
            sum += Character.getNumericValue(prodS.charAt(i));
        }
        System.out.println(prodS);
        System.out.println(sum);
    }
}
