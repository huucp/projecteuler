import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by huupc on 28/04/2017.
 */
public class NamesScores {
    /**
     * Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

     For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

     What is the total of all the name scores in the file?
     */
    public static void main(String[] args) {
        try {
            List<String> lines = Files.readAllLines(Paths.get("ProjectEuler/resources/p022_names.txt"));
            String[] names = lines.get(0).replaceAll("\"", "").split(",");
            Arrays.sort(names);
            BigInteger sum = BigInteger.ZERO;
            for (int i = 0; i < names.length; i++) {
                String name = names[i].toLowerCase();
//                name = "colin";
                int nSum =0;
                for (int j =0; j < name.length();j++) {
                    nSum += name.charAt(j) - 'a' +1;
                }
                sum = sum.add(BigInteger.valueOf(nSum * (i + 1)));
            }
            System.out.println(sum);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
