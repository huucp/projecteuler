import java.time.DayOfWeek;
import java.time.LocalDateTime;

/**
 * Created by huupc on 27/04/2017.
 */
public class CountinSundays_19 {
    /**
     * You are given the following information, but you may prefer to do some research for yourself.
     * <p>
     * 1 Jan 1900 was a Monday.
     * Thirty days has September,
     * April, June and November.
     * All the rest have thirty-one,
     * Saving February alone,
     * Which has twenty-eight, rain or shine.
     * And on leap years, twenty-nine.
     * A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
     * How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
     */
    public static void main(String[] args) {
        int count = 0;
        for (int year = 1901; year < 2001; year++) {
            for (int month = 1; month<13;month++) {
                LocalDateTime t = LocalDateTime.of(year, month, 1, 0, 0);

                if (t.getDayOfWeek() == DayOfWeek
                        .SUNDAY) {
                    count++;
                }
            }

        }
        System.out.println(count);
    }
}
