/**
 * Created by huupc on 05/05/2017.
 */
public class DigitFifthPowers_30 {
    /**
     * Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

     1634 = 1^4 + 6^4 + 3^4 + 4^4
     8208 = 8^4 + 2^4 + 0^4 + 8^4
     9474 = 9^4 + 4^4 + 7^4 + 4^4
     As 1 = 1^4 is not a sum it is not included.

     The sum of these numbers is 1634 + 8208 + 9474 = 19316.

     Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
     */

    /**SOLUTION
     * if we have n-digit number, then upper bound of sum of the fifth powers is n*9^5
     * if n>6 then upper bound is n*9^5<n*10^5<10^n =>n <=6 then upper bound of number is 6*9^5
     */


    public static void main(String[] args) {
        double uppper = Math.pow(9, 5) * 6;
        int result =0;
        for (int n =2; n <= uppper;n++) {
            int sumOfPowers = 0;
            int number =n;
            while (number > 0) {
                int d = number%10;
                number/=10;
                sumOfPowers+=(int)Math.pow(d,5);
            }
            if (sumOfPowers == n) {
                System.out.println(n);
                result+=sumOfPowers;
            }
        }
        System.out.println(result);
    }
}
