import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huupc on 26/04/2017.
 */
public class NthPrimeNumber {
    public static void main(String[] args) {
        System.out.println(findNthPrime(10001).toString());
    }


    public static BigInteger findNthPrime(int nth) {
        List<BigInteger> primes = new ArrayList<>();
        BigInteger b2 = BigInteger.valueOf(2);
        primes.add(b2);
        BigInteger lastPrime = BigInteger.valueOf(3);
        primes.add(lastPrime);

        boolean isPrime = true;
        while (primes.size() < nth) {
            lastPrime = lastPrime.add(b2);
            isPrime = true;

            for (BigInteger bi : primes) {
                if (bi.multiply(bi).compareTo(lastPrime) > 0) break;
                if (lastPrime.remainder(bi).compareTo(BigInteger.ZERO) == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                primes.add(lastPrime);
            }
        }
        return primes.get(nth - 1);
    }
}
