/**
 * Created by huupc on 25/04/2017.
 */
public class LargestPalindromeProduct {
    public static void main(String[] args) {
        System.out.println(new LargestPalindromeProduct().find());
    }

    // max palindrome is product of 3-digit number
    public int find() {
        int max = 0;
        for (int i = 999; i >= 0; i--) {
            for (int j = i; j >= 0; j--) {
                int res = i * j;
                if (checkPalindrome(Integer.toString(res))) {
                    if (max < res){
                        max = res;
                    }
                }
            }
        }
        return max;
    }

    private boolean checkPalindrome(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
