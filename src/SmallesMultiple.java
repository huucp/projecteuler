/**
 * Created by huupc on 26/04/2017.
 */
public class SmallesMultiple {
    public static void main(String[] args) {
        System.out.println(findLCM(20));
    }

    public static int findLCM(int max) {
        int lcm =1;
        for (int i = 2; i <= max; i++) {
            lcm = lcm(lcm, i);
        }
        return lcm;
    }


    private static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    private static int lcm(int a, int b) {
        return a * (b / gcd(a, b));
    }
}
