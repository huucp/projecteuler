import java.util.ArrayList;
import java.util.List;

/**
 * Created by huupc on 28/04/2017.
 */
public class NonAbudantSums_23 {
    /**
     * A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
     * <p>
     * A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
     * <p>
     * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
     * <p>
     * Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
     */
    public static void main(String[] args) {
        List<Integer> listAbudants = new ArrayList<>();
        long sum = 0;
        for (int i = 1; i <= 28123; i++) {
            if (isAbundantNumber(i)) {
                listAbudants.add(i);
            }
            boolean isSum = false;
            for (int j = 0; j < listAbudants.size(); j++) {
                for (int k = 0; k < listAbudants.size(); k++) {
                    if ((listAbudants.get(j) + listAbudants.get(k)) == i) {
                        isSum = true;
                        break;
                    }
                }
                if (isSum) break;
            }


            if (!isSum) {
                sum += i;
            }
        }
        System.out.println(sum);

    }

    private static boolean isAbundantNumber(int n) {
        if (n == 1) return false;
        int sum = 1;
        double sqrt = Math.sqrt(n);
        for (int i = 2; i < sqrt; i++) {
            if (n % i == 0) {
                sum += i;
                sum += (n / i);
            }
        }
        if (sqrt == (int) sqrt) {
            sum += sqrt;
        }
        return sum > n;
    }
}
