import java.math.BigInteger;

/**
 * Created by huupc on 25/04/2017.
 */
public class FactorialTrailingDigits2 {
    public static void main(String[] args) {

        FactorialTrailingDigits2 f = new FactorialTrailingDigits2();
        BigInteger factorial = f.factorial(f.factorial(BigInteger.valueOf(20)));
        System.out.println(f.find(factorial, 12));

    }


    /**
     * For any N, let f(N) be the last twelve hexadecimal digits before the trailing zeroes in N!.
     * <p>
     * For example, the hexadecimal representation of 20! is 21C3677C82B40000,
     * so f(20) is the digit sequence 21C3677C82B4.
     * <p>
     * Find f(20!). Give your answer as twelve hexadecimal digits, using uppercase for the digits A to F.
     */
    public String find(BigInteger n, int last) {
        String toHex = n.toString(16);
        for (int i = toHex.length() - 1; i >= last - 1; i--) {
            if (toHex.charAt(i) != '0') {
                StringBuilder sb = new StringBuilder();
                for (int j = i - (last - 1); j <= i; j++) {
                    sb.append(toHex.charAt(j));
                }
                return sb.toString();
            }
        }
        return null;
    }

    public BigInteger factorial(BigInteger n) {
        BigInteger res = BigInteger.ONE;
        BigInteger iter = BigInteger.ONE;
        while (iter.compareTo(n) <= 0) {
            res = res.multiply(iter);
            iter = iter.add(BigInteger.ONE);
        }
        return res;
    }
}